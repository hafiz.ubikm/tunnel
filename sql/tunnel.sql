-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 23, 2015 at 04:48 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tunnel`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_code`
--

CREATE TABLE IF NOT EXISTS `tbl_code` (
`ID` int(11) NOT NULL,
  `CODE` varchar(60) DEFAULT NULL,
  `OWNER_ID` int(11) DEFAULT NULL,
  `CREATED_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CODE_DURATION` tinyint(2) DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `STATUS` tinyint(2) DEFAULT NULL,
  `USED_FG` tinyint(1) DEFAULT '0' COMMENT '0=Unused 1=Used'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=86 ;

--
-- Dumping data for table `tbl_code`
--

INSERT INTO `tbl_code` (`ID`, `CODE`, `OWNER_ID`, `CREATED_DATE`, `CODE_DURATION`, `CREATED_BY`, `STATUS`, `USED_FG`) VALUES
(1, 'K34a2gRNuXokVcUVqKSugfjgjj', 19, '2015-08-21 20:40:13', 3, 1, 1, 1),
(2, 'K34a2gRNufdfdfeXokVcUVqKSu', 19, '2015-08-21 20:40:13', 5, 1, 1, 1),
(3, 'K34a2gRNuXokVcUVqKSu', 19, '2015-08-21 20:40:13', 5, 1, 1, 1),
(4, 'kKsFrbE6u6791WNfxMRc', 19, '2015-08-21 20:40:14', 7, 1, 1, 1),
(5, '4cmJkZ3Bu9f38c3A6xzl', 19, '2015-08-21 20:40:14', 7, 1, 1, 1),
(6, 'f2n62R1wb4iI6mWLyuHL', 4, '2015-08-21 19:22:48', 3, 1, 0, 1),
(7, 'oR1C1272Z3U97R4767O3', 4, '2015-08-21 19:22:43', 3, 1, 0, 1),
(8, 'WXLDREOuiGeF0SAPzywY', 6, '2015-08-21 20:21:23', 4, 1, 0, 1),
(9, 'lEYxCE8eN1lUkJVlK8bn', 6, '2015-08-21 20:21:23', 4, 1, 0, 0),
(10, 'yd5pIb980W9AaJhNtd88', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(11, 'pDHZ8onFy1cNqXfWK81l', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(12, 'ef6Bavq7BEzz5bJc4iET', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(13, 'JgXhjNUe923rOMwH1xK2', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(14, 'fXJT7Kj8hxaFXMjDL8Hi', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(15, '5DK4qDEvkO35Awt86iT2', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(16, 'm8O3b0DDPt192AXKW3H1', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(17, 'V38CVChJ6Uad18XEKHNJ', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(18, 'SdWJ53Qb2CFJmo7Z064Y', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(19, 'LOQNdsscoPY28eVJNlg8', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(20, 'l3gNwRrSpTJ1079Wsg58', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(21, 'hVn944HBU628LHhT8i76', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(22, 'eIGRUZ5TKW1jpmZ2b8Od', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(23, 'yz0hWAWQiE0lQ1En7HJT', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(24, 'u2gjW2d0mta6MhS1RALV', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(25, 'qAJ3iHYIYIwKRZJA5mRT', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(26, 'ALZ3FgzYZe3JX4pNfh6O', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(27, 'uFK5zst5HW0RKsdvIknD', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(28, '6MiNiC8nvezPowxEywUS', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(29, 'jQ9a0AhLexR15L6M5Mh9', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(30, 'w9T477svXZ21uk6bGv2y', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(31, 'Eh1VkU62ibw7fY9m4tNo', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(32, 'rzr5akcUR52hLmUz4L03', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(33, '83RUVM6Z7RVLz2U4oXMc', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(34, 'X2Bxx0BpW3Wk7FL64lQJ', 4, '2015-08-21 13:50:15', 4, 1, 0, 0),
(35, 'az6sUKRv1Ys336h1BTwX', 3, '2015-08-21 09:57:04', 4, 1, 0, 0),
(36, '9r5a32OAoukoas0F754M', 3, '2015-08-21 09:57:04', 4, 1, 0, 0),
(37, 'cbP4Bpa2ZVXQZKY9cj6t', 3, '2015-08-21 09:57:04', 4, 1, 0, 0),
(38, 'J7KqzK5Lt6E2cj5M4fmf', 3, '2015-08-21 09:57:05', 4, 1, 0, 0),
(39, '1fRfMnGUCJb9wrYJShY5', 3, '2015-08-21 09:57:05', 4, 1, 0, 0),
(40, '8grj45ZVfsiR1jeH29Fp', 3, '2015-08-21 09:57:05', 4, 1, 0, 0),
(41, 'HjDwC5sO4TpKX29vLpfg', 3, '2015-08-21 09:57:14', 4, 1, 0, 0),
(42, '6GYWWV8w9S4wy9iP5Je6', 3, '2015-08-21 09:57:14', 4, 1, 0, 0),
(43, '9wXGyUlYe6lzBxv8UMPV', 3, '2015-08-21 09:57:14', 4, 1, 0, 0),
(44, '4xx7v0pVz3tB4j117RRN', 3, '2015-08-21 09:57:14', 4, 1, 0, 0),
(45, 'm7bx5Lnn4i4X70PAd8Xq', 3, '2015-08-21 09:57:14', 4, 1, 0, 0),
(46, 'PIVA4tE5x1SgiCzdAtnj', 3, '2015-08-21 09:57:14', 4, 1, 0, 0),
(47, 'Y9Nrf7N5qf9BEfWT3yt1', 3, '2015-08-21 09:58:27', 5, 1, 0, 0),
(48, 'VyJAYi2c8S6WrAoF4GE9', 4, '2015-08-21 09:59:50', 2, 1, 0, 0),
(49, 'k1W8jid6rfwRXFBPec96', 4, '2015-08-21 09:59:50', 2, 1, 0, 0),
(50, 'Le56rc0H1YnlC0r7kB70', 4, '2015-08-21 09:59:50', 2, 1, 0, 0),
(51, 'cm5iCBj3NH9DyhMY275t', 4, '2015-08-21 09:59:50', 2, 1, 0, 0),
(52, 'EfaRLpXC2BbgLrv9y6SV', 4, '2015-08-21 09:59:50', 2, 1, 0, 0),
(53, 'X8YT0qQvtQ8IHLlylnX6', 4, '2015-08-21 10:00:48', 5, 1, 0, 0),
(54, '749iX63DarxjPIVA4tE5', 4, '2015-08-21 10:00:48', 5, 1, 0, 0),
(55, '68hvFymEKz72IrUU7BSG', 4, '2015-08-21 10:00:48', 5, 1, 0, 0),
(56, '2d58jtVPCH', 4, '2015-08-21 13:50:15', 2, 1, 0, 0),
(57, 'Eh8yH6iF1o', 4, '2015-08-21 13:50:15', 2, 1, 0, 0),
(58, 'AN1quI6AQm', 4, '2015-08-21 13:50:15', 2, 1, 0, 0),
(59, 'aY8qK57QRz', 4, '2015-08-21 13:50:15', 2, 1, 0, 0),
(60, 'IE19IH3n9t', 4, '2015-08-21 13:50:15', 2, 1, 0, 0),
(61, '4nG2UwmB914OPfGEqZkQteqR7Ew2uBCnxwOKpgWJI7nbj7i17B', 4, '2015-08-21 10:02:24', 1, 1, 0, 0),
(62, 'Acnqwj7J3e58Xuv2rn1Xqv4ACcI0F6S8yPrD5rfpJAC8Kx2oeS', 4, '2015-08-21 10:02:24', 1, 1, 0, 0),
(63, 'DgyeIO7XKSpY8vveHl6S4F6WOFu9C0KJG5a13BsbCW5rHEknDL', 4, '2015-08-21 10:02:25', 1, 1, 0, 0),
(64, '7a72d3X8y3zNa9uPN3Y2J9Jva90e9M1UlCkV866N9fH0YsJ1TS', 4, '2015-08-21 10:02:25', 1, 1, 0, 0),
(65, 'P2kdBy73BiIVNcEm0qsrq8eVmJ07ysglcpUlC4vWPtbQhn8Xh2', 4, '2015-08-21 10:02:25', 1, 1, 0, 0),
(66, 'zCT2F3wPm89y4vSIWkw5xfIM93BAlRDT3aV5Y9pc', 4, '2015-08-21 11:05:36', 6, 1, 0, 0),
(67, 'Z527JGqay51188DoV0f3FBe0dYzh8GCu994wGZ35', 4, '2015-08-21 11:05:36', 6, 1, 0, 0),
(68, '4cNL7BMBkHi83pyOUwNxLftinWfB15bJ82Bo4vS9', 4, '2015-08-21 11:05:37', 6, 1, 0, 0),
(69, 'PBkPgYBtN5Q8gploFRO7Q9gvJ8vxCOWxoaxG3uR4', 4, '2015-08-21 11:05:37', 6, 1, 0, 0),
(70, 'bxhRMAA8byBi40widZQKZVXqxi3zB5QH0F5sZBoB', 4, '2015-08-21 11:05:37', 6, 1, 0, 0),
(71, 'b717fUef6Bavq7BEzz5bJc4iETEpjyzXju76FU3s', 4, '2015-08-21 11:05:37', 6, 1, 0, 0),
(72, 'Na9uPN3Y2J9Jva90e9M1UlCkV866N9fH0YsJ1TS5', 4, '2015-08-21 11:05:37', 6, 1, 0, 0),
(73, 'J7KqzK5Lt6E2cj5M4fmfWrmM83RFZyOxaUTkS3k7', 4, '2015-08-21 11:05:37', 6, 1, 0, 0),
(74, 'HL5a48pA65HNNCye3mT9B7THuIMfJ2c2RU6IDIS1', 4, '2015-08-21 11:05:37', 6, 1, 0, 0),
(75, 'iMtN66mRtFwx57D3P3AlNnQw8OhC3ZQjw3VEV7Nw', 4, '2015-08-21 11:05:37', 6, 1, 0, 0),
(76, 'I5eeQ4X3Rr93W4L74tAP', 3, '2015-08-21 11:16:01', 3, 1, 0, 0),
(77, '2Bhfp1f8l9A72vLrdbEt', 3, '2015-08-21 11:16:01', 3, 1, 0, 0),
(78, 'M45M4c33nK6tO5JG2CTK', 3, '2015-08-21 11:16:01', 3, 1, 0, 0),
(79, '6uw61PTQh632LB6Z5uta', 3, '2015-08-21 11:16:01', 3, 1, 0, 0),
(80, 'g3Mq2TAR96j4OzjUi54I', 3, '2015-08-21 11:16:01', 3, 1, 0, 0),
(81, '53wbkm8OAP6w2WM3Prk3', 3, '2015-08-21 11:16:01', 3, 1, 0, 0),
(82, 'dW3ai85Tnah4CyPrq7Y6', 3, '2015-08-21 11:16:01', 3, 1, 0, 0),
(83, '754M77Fp6otI8y4xQr33', 3, '2015-08-21 11:16:01', 3, 1, 0, 0),
(84, 'Ux5p956JZP77Y9nBK1ke', 3, '2015-08-21 11:16:01', 3, 1, 0, 0),
(85, 'RqTFpqPb717fUef6Bavq', 3, '2015-08-21 11:16:01', 3, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_information`
--

CREATE TABLE IF NOT EXISTS `tbl_user_information` (
`ID` int(11) NOT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `EMAIL` varchar(200) DEFAULT NULL,
  `USERNAME` varchar(50) DEFAULT NULL,
  `PASSWORD` varchar(100) DEFAULT NULL,
  `PHONE` varchar(20) DEFAULT NULL,
  `ADDRESS` text,
  `CREATED_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `UPDATED_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `UPDATED_BY` int(11) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL COMMENT '1=Active 2=Inactive',
  `EXPIRE_DATE` datetime DEFAULT NULL,
  `USER_TYPE` char(2) DEFAULT 'U' COMMENT 'U=User R=Reseller A=Admin',
  `REFERRAL_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `tbl_user_information`
--

INSERT INTO `tbl_user_information` (`ID`, `NAME`, `EMAIL`, `USERNAME`, `PASSWORD`, `PHONE`, `ADDRESS`, `CREATED_DATE`, `CREATED_BY`, `UPDATED_DATE`, `UPDATED_BY`, `STATUS`, `EXPIRE_DATE`, `USER_TYPE`, `REFERRAL_ID`) VALUES
(1, 'Admin', 'hafiz.ubikm@gmail.com', 'admin', '12345', '01926743629', 'Dhanmondi', '2015-08-20 02:30:04', 1, '0000-00-00 00:00:00', NULL, 1, NULL, 'A', NULL),
(2, 'Admin 1', 'hafiz.ubikm@gmail.com', 'admin1', '12345', '01926743629', 'Mohammadpur', '2015-08-20 02:40:05', 1, '0000-00-00 00:00:00', NULL, 1, NULL, 'A', NULL),
(3, 'Admin 2', 'hafiz.ubikm@gmail.com', 'admin2', '12345', '01926743629', 'Mohammadpur', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', NULL, 1, NULL, 'A', NULL),
(4, 'Reseller', 'reseller@gmail.com', 'reseller', '12345', '01926743629', NULL, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', NULL, 1, NULL, 'R', NULL),
(5, 'Reseller 1', 'reseller@gmail.com', 'reseller1', '12345', '01926743629', 'Dhaka', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', NULL, 0, NULL, 'R', NULL),
(6, 'Reseller 2', 'reseller@gmail.com', 'reseller2', '12345', '01926743629', 'Dhaka', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', NULL, 0, NULL, 'R', NULL),
(7, 'Shofiq', 'hafiz.ubikm@gmail.com', 'shofiq', '123', '01926743629', '123', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', NULL, 1, NULL, 'U', NULL),
(8, 'Noman', 'hafiz.ubikm@gmail.com', 'noman', '123', '01926743629', '123', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', NULL, 0, NULL, 'U', NULL),
(9, 'Kamal', 'kamal@gmail.com', 'kamal', '123', '01926743629', 'dhanmondi', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', NULL, 0, NULL, 'U', NULL),
(10, 'Hafizur Rhaman', 'hafiz.ubikm@gmail.com', 'hafiz', 'dhaka', '01926743629', '12345', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', NULL, 0, NULL, 'U', NULL),
(11, 'Hafizur Rhaman', 'hafiz.ubikm@gmail.com', 'hafiz', 'dhaka', '01926743629', '12345', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', NULL, 0, NULL, 'U', NULL),
(12, 'Kamall', 'hafiz.ubikm@gmail.com', 'kamal', '123', '01926743629', 'Khulna', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', NULL, 0, NULL, 'U', NULL),
(13, 'Kamall', 'hafiz.ubikm@gmail.com', 'kamal', '123', '01926743629', 'Khulna', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', NULL, 0, NULL, 'U', NULL),
(14, 'Test', 'hafiz.ubikm@gmail.com', 'test', '12345', '01926743629', 'dhaka', '2015-08-21 20:34:52', 1, '0000-00-00 00:00:00', NULL, 1, NULL, 'U', NULL),
(16, 'User Test', 'usertest@gmail.com', 'usertest', '98765', '01926743629', 'Dhaka', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', NULL, 1, NULL, 'R', NULL),
(17, 'Sumon', 'sumon@gmail.com', 'sumon', '12345', '12345', 'dhaka', '2015-08-21 18:10:58', 1, '0000-00-00 00:00:00', NULL, 0, NULL, 'R', NULL),
(18, 'ksjalkfj', 'lksalf@slkfjas.com', 'tests', '12345', '23423423', 'dhaka', '2015-08-21 18:10:56', 1, '0000-00-00 00:00:00', NULL, 1, NULL, 'R', NULL),
(19, 'skfjlkj', 'skfjlsakfj@lasfjs.com', 'skjfksallsfkj', '123456789', '123456789', 'gazipur', '2015-08-21 20:40:14', 1, '0000-00-00 00:00:00', NULL, 1, NULL, 'U', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_code`
--
ALTER TABLE `tbl_code`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_user_information`
--
ALTER TABLE `tbl_user_information`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_code`
--
ALTER TABLE `tbl_code`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `tbl_user_information`
--
ALTER TABLE `tbl_user_information`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
