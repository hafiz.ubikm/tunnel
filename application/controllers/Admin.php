<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    
    public function index(){
        $this->load->view('signin');
    }
    
    public function check_user(){
        $username    = $this->input->post('username');
        $password = $this->input->post('user_password');
        $query    = $this->db->get_where('tbl_user_information', array(
            'USERNAME' 	=> $username,
            'PASSWORD' 	=> $password,
            'STATUS'	=>1
        ));
        if ($query->num_rows() > 0){
            $userinfo = $query->row();
            $userdata = array(
                'name'      => $userinfo->NAME,
                'id'        => $userinfo->ID,
                'user_type' => $userinfo->USER_TYPE
            );
            $this->session->set_userdata($userdata);
            redirect('admin/dashboard');
        }
        else {
            $this->session->set_flashdata('message_name', '<p class="alert-danger" style="padding:10px; border-radius:5px">Username or Password is not valid.</p>');
            // After that you need to used redirect function instead of load view such as 
            redirect("admin/index");
        }
    }


    public function dashboard(){
        $query              = "SELECT * FROM tbl_user_information where ID=".$this->session->userdata('id');;
        $record             = $this->db->query($query);
        $data['get_record'] = $record->row_array();
        $this->load->view('tunnel/dashboard', $data); 
    }
    
    public function display_user(){

        $query              = "SELECT * FROM tbl_user_information";
        $record             = $this->db->query($query);
        $data['get_record'] = $record->result();
        $this->load->view('tunnel/index', $data);
    }
    
    public function create_user(){
        $query              = "SELECT * FROM tbl_user_information";
        $record             = $this->db->query($query);
        $data['get_record'] = $record->result();
        $this->load->view('tunnel/create_user',$data);
    }
    
    
    public function register(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('register_name', 'register_name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('register_username', 'register_username', 'required|trim|xss_clean|is_unique[tbl_user_information.USERNAME]');
        $this->form_validation->set_rules('register_email', 'register_email', 'required|valid_email|trim|xss_clean');
        $this->form_validation->set_rules('register_password', 'register_password', 'required|trim|xss_clean');
        $this->form_validation->set_rules('register_phone', 'register_phone', 'required|trim|xss_clean');
        $this->form_validation->set_rules('register_address', 'register_address', 'required|trim|xss_clean');
        $this->form_validation->set_rules('user_type', 'user_type', 'required|trim|xss_clean');
        
        if (!$this->form_validation->run()) {
            $this->load->view('tunnel/create_user');
            return FALSE;
        }
        
        #create user
        $data = array(
            'NAME'          => $this->input->post('register_name'),
            'EMAIL'         => $this->input->post('register_email'),
            'USERNAME'      => $this->input->post('register_username'),
            'PASSWORD'      => $this->input->post('register_password'),
            'PHONE'         => $this->input->post('register_phone'),
            'ADDRESS'       => $this->input->post('register_address'),
            'CREATED_BY'    => $this->session->userdata('id'),
            'USER_TYPE'     => $this->input->post('user_type'),
            'STATUS'        => ($this->input->post('user_type')=='U') ? 0 : 1,
            'REFERRAL_ID'   => $this->session->userdata('id')
        );
        $this->db->insert('tbl_user_information', $data);
        $this->display_user();
    }

    public function load_create_code(){
        $query              = "SELECT * FROM tbl_user_information WHERE USER_TYPE !='U'";
        $record             = $this->db->query($query);
        $data['get_record'] = $record->result();
        $this->load->view('tunnel/code_genarete', $data);
    }
    
    public function generate_code(){
        function randomPrefix($length){
            $random = "";
            srand((double) microtime() * 1000000);
            
            $data  = "AbcDE123IJKLMN67QRSTUVWXYZ";
            $data .= "aBCdefghijklmn123opq45rs67tuv89wxyz";
            $data .= "0FGH45OP89";
            
            for ($i = 0; $i < $length; $i++) {
                $random .= substr($data, (rand() % (strlen($data))), 1);
            }
            return $random;
        }
        
        
        for ($i = 0; $i < $this->input->post('numberofcode'); $i++) {
            $code   = randomPrefix($this->input->post('codesize'));
            $numrow = $this->db->get_where('tbl_code', array(
                'CODE' => $code
            ))->num_rows();
            if ($numrow > 0) {
                $code = randomPrefix($this->input->post('codesize'));
            }

            $data = array(
                'CODE'          => $code,
                'OWNER_ID'      => $this->input->post('codefor'),
                'CODE_DURATION' => $this->input->post('duration'),
                'CREATED_BY'    => $this->session->userdata('id'),
                'STATUS'        => 0
            );
            $this->db->insert('tbl_code', $data);
        }
        redirect('admin/load_create_code');
    }
    


    public function display_code(){
    	$owner=$this->session->userdata('id');
    	if($this->session->userdata('user_type')!='A'){
       	 	$query= "SELECT tbl_code.*, tbl_user_information.NAME FROM tbl_code INNER JOIN tbl_user_information ON tbl_user_information.ID=tbl_code.OWNER_ID WHERE OWNER_ID=".$owner;
    	}else{
    		$query= "SELECT tbl_code.*, tbl_user_information.NAME FROM tbl_code INNER JOIN tbl_user_information ON tbl_user_information.ID=tbl_code.OWNER_ID";
    	}
        $record             = $this->db->query($query);
        $data['get_record'] = $record->result();
        $data['get_user']=$this->db->query("SELECT * FROM tbl_user_information WHERE USER_TYPE !='A' AND ID !='".$this->session->userdata('id')."'")->result();
        $this->load->view('tunnel/display_code', $data);
    }
    
    public function Logout(){
        session_unset();
        session_destroy();
        redirect('admin/index');
        
    }


    public function display_unused_code(){
        $owner=$this->session->userdata('id');
        if($this->session->userdata('user_type')!='A'){
            $query= "SELECT * FROM tbl_code where OWNER_ID='".$owner."' AND USED_FG=0";
        }else{
            $query= "SELECT tbl_code.*, tbl_user_information.NAME FROM tbl_code INNER JOIN tbl_user_information ON tbl_user_information.ID=tbl_code.OWNER_ID where USED_FG=0";
        }
        $record             = $this->db->query($query);
        $data['get_record'] = $record->result();
        $data['get_user']=$this->db->query("SELECT * FROM tbl_user_information WHERE USER_TYPE !='A'")->result();
        $this->load->view('tunnel/display_code', $data);
    }


    public function display_used_code(){
        $owner=$this->session->userdata('id');
        if($this->session->userdata('user_type')!='A'){
            $query= "SELECT * FROM tbl_code where OWNER_ID='".$owner."' AND USED_FG=1";
        }else{
            $query= "SELECT tbl_code.*, tbl_user_information.NAME FROM tbl_code INNER JOIN tbl_user_information ON tbl_user_information.ID=tbl_code.OWNER_ID where USED_FG=1";
        }
        $record             = $this->db->query($query);
        $data['get_record'] = $record->result();
        $data['get_user']=$this->db->query("SELECT * FROM tbl_user_information WHERE USER_TYPE !='A'")->result();
        $this->load->view('tunnel/display_code', $data);
    }

    public function transfer_code(){

        $id=$this->input->post('codeid');
        $user=$this->input->post('codefor');

        foreach($id as $value){
            $this->db->query("UPDATE tbl_code SET OWNER_ID='".$user."', USED_FG=1, STATUS=1 WHERE ID='".$value."'");
        }
        $this->db->query("UPDATE tbl_user_information SET STATUS=1 WHERE ID='".$user."'");
        $this->load->library('user_agent');
        redirect($this->agent->referrer());
    }
    
}