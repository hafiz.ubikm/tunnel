<br><div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <div class="panel panel-primary" style="margin-left:5%">
            <div class="panel-heading">
                <h4>Operations</h4>
                <?php $usrType=$this->session->userdata('user_type');?>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <ul class="nav" id="side-menu">
        			
                    <li>
                        <a href="<?php echo site_url('admin/dashboard');?>"><b><i class="fa fa-dashboard fa-fw"></i> Dashboard</b></a>
                    </li>
                    <?php if($usrType==='A'): ?>
                    <li>
                        <a href="<?php echo site_url('admin/create_user');?>"><b><i class="fa fa-bar-chart-o fa-fw"></i> Create User</b></a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('admin/display_user');?>"><b><i class="fa fa-bar-chart-o fa-fw"></i> Manage User</b></a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('admin/load_create_code');?>"><b><i class="fa fa-table fa-fw"></i> Create Voucher</b></a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('admin/display_code');?>"><b><i class="fa fa-bar-chart-o fa-fw"></i> Manage Voucher</b></a>
                    </li>
                    <?php endif;?>

                    <?php if($usrType==='A' || $usrType==='R'): ?>
                    <li>
                        <a href="<?php echo site_url('admin/display_used_code');?>"><b><i class="fa fa-table fa-fw"></i> Used Voucher</b></a>
                    </li>  
                    <li>
                        <a href="<?php echo site_url('admin/display_unused_code');?>"><b><i class="fa fa-table fa-fw"></i> Unused Voucher</b></a>
                    </li>  
                    <li>
                        <a href="<?php echo site_url('admin/display_unused_code');?>"><b><i class="fa fa-table fa-fw"></i> Transfer Voucher</b></a>
                    </li> 

                <?php endif;?>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>