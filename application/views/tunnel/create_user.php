<?php include_once('header.php');?> 
<?php include_once('sidebar.php');?>
        <div id="page-wrapper">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4>Create Users</h4>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div id="register_panel">
                           <form id="frm_register" action="<?php echo site_url('register');?>" method="post" data-parsley-validate>
                              <div class="form-group col-md-6">
                                <font color="red"><?php echo validation_errors(); ?></font>
                                 <label>Name*</label>
                                 <div>
                                    <input type="text" class="input_full form-control" id="register_name" name="register_name" value="<?php echo set_value('register_name'); ?>" placeholder="Please enter Name" required />
                                 </div>
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Email Address*</label>
                                 <div>
                                    <input type="email" class="input_full form-control" id="register_email" name="register_email" value="<?php echo set_value('register_email'); ?>" placeholder="Please enter Email" required />
                                 </div>
                              </div>
                              <div class="form-group col-md-6">
                                 <label>User Name*</label>
                                 <div>
                                    <input type="text" class="input_full form-control" id="register_username" name="register_username" value="<?php echo set_value('register_username'); ?>" placeholder="Please enter User Name" required />
                                 </div>
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Password*</label>
                                 <div>
                                    <input type="password" class="input_full form-control" id="register_password" name="register_password" value="" placeholder="Please enter Password" required />
                                 </div>
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Re-type Password*</label>
                                 <div>
                                    <input type="password" class="input_full form-control" id="register_password_again" name="register_password_again" value="" placeholder="Please re-type Password" data-parsley-equalto="#register_password" />
                                 </div>
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Phone Number*</label>
                                 <div>
                                    <input type="text" class="input_full form-control" id="register_phone" name="register_phone" value="<?php echo set_value('register_phone'); ?>" placeholder="Please enter Phone Number" required />
                                 </div>
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Mailing Address*</label>
                                 <div>
                                    <textarea class="input_full  form-control" id="register_address" min="1" max="250" name="register_address" value="<?php echo set_value('register_address'); ?>" placeholder="Please enter Address" required><?php echo set_value('register_address'); ?></textarea>
                                 </div>
                              </div>
                              <div class="form-group col-md-6">
                                 <label>User Type*</label>
                                 <div>
                                   <select class="form-control" name="user_type">
                                      <option value="U" selected>User</option>
                                      <option value="R">Reseller</option>
                                   </select>
                                 </div>
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Referral User*</label>
                                 <div>
                                  <input list="userList" name="referralUser" class="form-control" placeholder="Select A User">
                                  <datalist id="userList">
                                    <?php foreach ($get_record as $key => $value) { ?>
                                    <option value="<?php echo $value->NAME?>"></option>
                                    <?php }?>
                                  </datalist>
                                 </div>
                              </div>
                              <div class="form-group col-md-6">
                                 <input type="submit" class="btn btn-danger" value="Register" />
                              </div>
                           </form>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
            <!-- /.panel -->
            </div>
        </div>
<?php include_once('footer.php');?>