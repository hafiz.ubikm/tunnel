<?php include_once('header.php');?> 
<?php include_once('sidebar.php');?>	
        <div id="page-wrapper">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4>Manage Voucher</h4>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <form method="post" action="<?php echo site_url('admin/transfer_code');?>">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr class="success">
                                        <th><input type="checkbox" id="selecctall"></th>
                                        <th>Code</th>
                                        <?php if($this->session->userdata('user_type')=='A'):?>
                                        <th>Owner</th>
                                        <?php endif?>
                                        <th>Duration</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($get_record as $k=>$v){?>
                                    <tr>
                                        <td><input type="checkbox" class="codeid" name="codeid[]" value="<?php echo $v->ID;?>"></td>
                                        <td><?php echo $v->CODE; ?></td>
                                        <?php if($this->session->userdata('user_type')=='A'):?>
                                        <th><?php echo $v->NAME; ?></th>
                                        <?php endif;?>
                                        <td><?php echo $v->CODE_DURATION; ?></td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                            <?php if($this->uri->segment(2)==='display_unused_code'){ ?>
                                <div class="row">
                                <div class="col-md-3">
                                <select  name="codefor" class="form-control" id="codefor" required>
                                  <option value="">Select User</option>
                                  <?php foreach($get_user as $k=>$v1){?>
                                  <option value="<?php echo $v1->ID?>"><?php echo $v1->NAME?></option>
                              <?php }?>
                            </select>
                        </div>
                                    <div class="col-md-4">
                                <input type="submit" value="Transfer Code" class="btn btn-success"> 
                            </div>
                        <div class="col-md-5">
                        </div>
                    </div>
                        <?php }?>
                        </form>
                        </div>
                    </div>
                    <!-- /.panel-body -->

                </div>
            <!-- /.panel -->
            </div>
        </div>
<?php include_once('footer.php');?>