<?php include_once('header.php');?> 
<?php include_once('sidebar.php');?>	
        <div id="page-wrapper">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4>All Users</h4>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr class="success">
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>User Name</th>
                                        <th>Phone</th>
                                        <th>User Type</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($get_record as $k=>$v){?>
                                    <tr class="gradeU">
                                        <td><?php echo $v->NAME; ?></td>
                                        <td><?php echo $v->EMAIL; ?></td>
                                        <td><?php echo $v->USERNAME; ?></td>
                                        <td class="center"><?php echo $v->PHONE; ?></td>
                                        <td class="center">
                                            <?php $userType= $v->USER_TYPE;
                                                if($userType=='A'){
                                                    echo "Admin";
                                                }elseif($userType=='R'){
                                                    echo "Reseller";
                                                }else{
                                                    echo "User";
                                                }
                                             ?>
                                        </td>
                                        <td class="center">
                                            <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a> 
                                            <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a> 
                                            <?php 
                                                $status=$v->STATUS; 
                                                if($status==1){
                                                    echo '<a href="#" class="btn btn-warning btn-xs">Inctive</a>';
                                                }else{
                                                    echo '<a href="#" class="btn btn-success btn-xs">Active</a>';
                                                }
                                            ?> 
                                        </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
            <!-- /.panel -->
            </div>
        </div>
<?php include_once('footer.php');?>