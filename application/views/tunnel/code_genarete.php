<?php include_once('header.php');?> 
<?php include_once('sidebar.php');?>
        <div id="page-wrapper">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4>Code Generate</h4>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div id="register_panel">
                           <form id="frm_register" action="<?php echo site_url('admin/generate_code');?>" method="post" data-parsley-validate/>
                              <div class="col-md-6">
                              <div class="form-group">
                                 <label>Code For*</label>
                                 <div>
                                    <select class="form-control" name="codefor" id="codefor" required>
                                      <option value="">Select User</option>
                                      <?php foreach($get_record as $k=>$v){?>
                                      <option value="<?php echo $v->ID?>"><?php echo $v->NAME?></option>
                                      <?php }?>
                                    </select>
                                 </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                 <label>Code Duration*</label>
                                 <div>
                                      <select class="form-control" name="duration" id="duration" required>
                                        <option value="">Select Code Duration</option>
                                        <option value="1">01 Month</option>
                                        <option value="2">02 Months</option>
                                        <option value="3">03 Months</option>
                                        <option value="4">04 Months</option>
                                        <option value="5">05 Months</option>
                                        <option value="6">06 Months</option>
                                        <option value="7">07 Months</option>
                                        <option value="8">08 Months</option>
                                        <option value="9">09 Months</option>
                                        <option value="10">10 Months</option>
                                        <option value="11">11 Months</option>
                                        <option value="12">12 Months</option>
                                    </select>
                                 </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                 <label>Code Size*</label>
                                 <div>
                                    <input type="number" class="form-control" id="codesize" name="codesize" value="<?php echo set_value('register_username'); ?>" placeholder="Please enter Code size" required data-parsley-range="[10,60]"/>
                                 </div>
                              </div>
                              </div>
                              <div class="col-md-6">
                              <div class="form-group">
                                 <label>Number of Code*</label>
                                 <div>
                                    <input type="number" class="form-control" id="numberofcode" name="numberofcode" value="<?php echo set_value('register_username'); ?>" placeholder="Please enter Number of code" required />
                                 </div>
                              </div>
                              </div>
                              <div class="col-md-6">
                              </div>
                              <div class="col-md-6">
                              <div class="form-group">
                                 <input type="submit" class="btn btn-danger" value="Generate Code" />
                              </div>
                              </div>
                           </form>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
            <!-- /.panel -->
            </div>
        </div>
<?php include_once('footer.php');?>