<?php include_once('header.php');?> 
<?php include_once('sidebar.php');?>	
        <div id="page-wrapper">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <?php $data= extract($get_record);?>
                        <h4>Profile Details of <?php echo $NAME;?></h4>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="col-md-6">
                        <table class="table table-striped">
                            
                            <tr>
                                <th>Name</th>
                                <td><?php echo "<b>: </b>".$NAME;?></td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td><?php echo "<b>: </b>".$EMAIL;?></td>
                            </tr>
                            <tr>
                                <th>User Name</th>
                                <td><?php echo "<b>: </b>".$USERNAME;?></td>
                            </tr>
                            <tr>
                                <th>Phone</th>
                                <td><?php echo "<b>: </b>".$PHONE;?></td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td><?php echo "<b>: </b>".$ADDRESS;?></td>
                            </tr>
                            <tr>
                                <th>Created Date</th>
                                <td><?php echo "<b>: </b>".$CREATED_DATE;?></td>
                            </tr>
							<?php if($this->session->userdata('user_type')!='A'){?>
                            <tr>
                                <th>Expire Date</th>
                                <td><?php echo "<b>: </b>".$EXPIRE_DATE;?></td>
                            </tr>
							<?php } ?>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <img src="<?php echo base_url('images/profile.jpg')?>" class="image-responsiv" width="100%">
                    </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
            <!-- /.panel -->
            </div>
        </div>
<?php include_once('footer.php');?>