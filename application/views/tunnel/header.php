<!DOCTYPE html>
<html>
<head>
    <title>WI Freedom</title>
    <link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('css/metisMenu.min.css');?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('css/sb-admin-2.css');?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('css/font-awesome.min.css');?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('css/dataTables.bootstrap.css');?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('css/dataTables.responsive.css');?>" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="http://itpointbd.com/recruitment/assets/css/parsley.css">
    <style>
    .btn{
        border-radius: 0px;
    }
    </style>

   <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
	<body>
	<div id="wrapper">
		<div class="navbar navbar-default navbar-static-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Responsive Navigation</span>
                    <span class="icon-bar "></span>
                    <span class="icon-bar "></span>
                    <span class="icon-bar "></span>
                    <span class="icon-bar "></span>
                </button>
				<a class="navbar-brand">We Freedom</a>
            </div>

			<ul class="nav navbar-top-links navbar-right">
				<li>
					<a><i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i></a>
				</li>
				<li>
					<a><i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i></a>
				</li>
				<li>
					<a><i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i></a>
				</li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> <?php echo  $this->session->userdata('name');?></a>
                        </li>
                        <li><a href="<?php echo site_url('admin/dashboard')?>"><i class="fa fa-file fa-fw"></i> Your Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url('admin/Logout');?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </li>
			</ul>
		