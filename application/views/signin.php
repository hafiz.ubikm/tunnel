<!DOCTYPE html>
<html lang="en">
   <head>
      <title>WI Freedom</title>
      <meta name="keywords" content="" />
      <meta name="description" content="" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
      <link rel="stylesheet" href="http://itpointbd.com/recruitment/assets/css/parsley.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="http://itpointbd.com/recruitment/assets/js/parsley.min.js"></script>
      <style>
         .login_footer { 
            border-top:1px solid #C9302C; 
            width:50%; 
            min-width:200px; 
            text-align:center; 
            color:#C9302C; 
            margin:auto; 
         }
         .panel {
            box-shadow:0 0 10px #222222;
         }
      </style>
   </head>
   <body>
      <div id="wrapper" style="background:none;"><br></br><br></br><br></br><br></br>
         <div id="page_signin" class="page_identifier container">
            <div class="row">
               <div class="col-md-4 col-md-offset-4">
                  <div class="text-center">
                     <img src=""/>
                  </div>
                  <br />
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h3 class="panel-title">User Authentication</h3>
                     </div>
                     <div class="panel-body panel-body-o">
                        <?php echo $this->session->flashdata('message_name');?>
                        <!-- end error message -->
                        <div id="login_panel" style="">
                           <form id="frm_signin" action="<?php echo site_url('admin/check_user')?>" method="post" data-parsley-validate>
                              <div class="form-group">
                                 <label for="user_id">User Name*</label>
                                 <div>
                                    <input type="text" class="form-control" id="username" name="username" value="" placeholder="Enter your User name" required />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="login_password">Password*</label>
                                 <div>
                                    <input type="password" class="form-control" id="user_password" name="user_password" value="" placeholder="Enter your password" required />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <input type="submit" class="btn btn-danger" value="Login" />
                              </div>
                           </form>
                        </div>
                        <!--end #login_panel-->
                     </div>
                  </div>
                  <!-- end .panel -->
               </div>
            </div>
            <!-- end .row -->
         </div>
         <!--end #page_signin-->
         <h3 class="login_footer">Copyright &copy; 2015</h3>
      </div>
   </body>
</html>